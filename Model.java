package gestion_podcasts_v2.Model;

import com.mysql.cj.xdevapi.Statement;
import java.util.prefs.Preferences;
import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;


public class Model {
    
    //session
    private static Preferences preferences = Preferences.userNodeForPackage(Model.class);

    //session
    
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/gestion_podcats";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    // Function to establish a connection to the database
    public static Connection connect() {
        Connection connection = null;
        try {
            // Establish connection
            connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
            System.out.println("Connected to the database!");
        } catch (SQLException e) {
            System.err.println("Failed to connect to the database!");
            e.printStackTrace();
        }
        return connection;
    }

    // Test method to check if the connection works
    public static void testConnection() {
        Connection connection = connect();
        if (connection != null) {
            try {
                connection.close();
                System.out.println("Connection closed successfully!");
            } catch (SQLException e) {
                System.err.println("Failed to close connection!");
                e.printStackTrace();
            }
        }
    }

    
     public static void  removeDataFromSession(String key) {
        preferences.remove(key);
    }
    
    // Method to insert data into the database
    public static void insertPodcast( String title, int id_utilisateur , String image ,  String description, String podacstPath ) {
        Connection connection = connect();
        if (connection != null) {
            try {
                // Prepare the SQL INSERT statement
                String sql = "INSERT INTO podcasts (title, id_utilisateur, image, description , url_podcast) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setString(1, title);
                statement.setInt(2, id_utilisateur);
                statement.setString(3, image);
                statement.setString(4, description);
                statement.setString(5, podacstPath);
                

                // Execute the INSERT statement
                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("Data inserted successfully!");
                } else {
                    System.out.println("Failed to insert data.");
                }

                // Close resources
                statement.close();
                connection.close();
            } catch (SQLException e) {
                System.err.println("Failed to insert data!");
                e.printStackTrace();
            }
        }
    }

    // Method to retrieve all podcasts from the database
    
     public static List<Object[]> getAllPodcasts() {
        List<Object[]> podcasts = new ArrayList<>();
        Connection connection = connect();
        if (connection != null) {
            try {
                // Prepare the SQL SELECT statement
                String sql = "SELECT * FROM podcasts";
                PreparedStatement statement = connection.prepareStatement(sql);

                // Execute the SELECT statement
                ResultSet resultSet = statement.executeQuery();

                // Process the result set
                while (resultSet.next()) {
                    Object[] podcastData = new Object[5];
                    podcastData[0] = resultSet.getString("image");
                    podcastData[1] = resultSet.getString("title");
                    podcastData[2] = resultSet.getString("description");
                    podcastData[3] = resultSet.getString("url_podcast");
                    podcastData[4] = resultSet.getInt("id_podcasts");
                    podcasts.add(podcastData);
                }

                // Close resources
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                System.err.println("Failed to retrieve podcasts!");
                e.printStackTrace();
            }
        }
        return podcasts;
    }
     
     
     
    public static List<Object[]> getMyPodcasts(String email) {
    List<Object[]> podcasts = new ArrayList<>();
    Connection connection = connect();
    if (connection != null) {
        try {
            // Prepare the SQL SELECT statement
            String sql = "SELECT * FROM podcasts p, utilisateur u WHERE p.id_utilisateur = u.id_utilisateur AND u.adress_email = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            
            // Set the user ID parameter in the PreparedStatement
            statement.setString(1, email);

            // Execute the SELECT statement
            ResultSet resultSet = statement.executeQuery();

            // Process the result set
            while (resultSet.next()) {
                ImageIcon imageIcon = loadImageIcon(resultSet.getString("image"));
                Object[] podcastData = new Object[5];
                podcastData[0] = imageIcon;
                podcastData[1] = resultSet.getString("title");
                podcastData[2] = resultSet.getString("description");
                podcastData[3] = "Delete"; // Just add a string for now
                podcastData[4] = resultSet.getInt("id_podcasts");
                podcasts.add(podcastData);
            }

            // Close resources
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to retrieve podcasts!");
            e.printStackTrace();
        }
    }
    return podcasts;
}

    
    public static List<Object[]> getMyFavorisPodcasts(int id) {
    List<Object[]> podcasts = new ArrayList<>();
    Connection connection = connect();
    if (connection != null) {
        try {
            // Prepare the SQL SELECT statement
            String sql = "SELECT * FROM favoris WHERE id_utilisateur=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            
            // Set the user ID parameter in the PreparedStatement
            statement.setInt(1, id);

            // Execute the SELECT statement
            ResultSet resultSet = statement.executeQuery();

            // Process the result set
            while (resultSet.next()) {
                ImageIcon imageIcon = loadImageIcon(resultSet.getString("image"));
                Object[] podcastData = new Object[5];
                podcastData[0] = imageIcon;
                podcastData[1] = resultSet.getString("title");
                podcastData[2] = resultSet.getString("description");
                podcastData[3] = "Delete"; // Just add a string for now
                podcastData[4] = resultSet.getInt("id_podcasts");
                podcasts.add(podcastData);
            }

            // Close resources
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to retrieve podcasts!");
            e.printStackTrace();
        }
    }
    return podcasts;
}

    
    private static ImageIcon loadImageIcon(String imageName) {
        // Load the image from resources or file system
        // Return ImageIcon object
        return new ImageIcon(imageName);
    }
    
    public static void main(String[] args) {
        
//        Model sessionManager = new Model();
//        sessionManager.startSession();        
//        removeDataFromSession("id_user");

        
//        sessionManager.addIdToSEssion("id_user", "1");

//        String id = sessionManager.getData("id_user");
//        System.out.println("id is : " + id);
        
    try {
        List<String[]> podcasts = new ArrayList<>();

        if (!podcasts.isEmpty()) {
            String[] firstPodcast = podcasts.get(0);
            System.out.println("First Podcast:");
            System.out.println("Duration: " + firstPodcast[0]);
            System.out.println("Title: " + firstPodcast[1]);
            System.out.println("User ID: " + firstPodcast[2]);
            System.out.println("Image: " + firstPodcast[3]);
        } else {
            System.out.println("No podcasts found.");
        }
    } catch (Exception e) {
        System.err.println("An error occurred: " + e.getMessage());
        e.printStackTrace();
    }
}
    
    

            // Method to delete a podcast from the database by its ID
        
    public static void deletePodcast(int idPodcast) {
    try {
        Connection conn = connect();
        String sql = "DELETE FROM podcasts WHERE id_podcasts = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setInt(1, idPodcast);
        pstmt.executeUpdate();
        pstmt.close();
        conn.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
    
    
    

        
        //session
        
        public static void startSession(){
        
          preferences = Preferences.userNodeForPackage(Model.class);
        
        }
        
         public static void addIdToSEssion(String key, String value) {
           // Store the key-value pair in preferences
           preferences.put(key, value);
        }
        
        public static String getData(String key) {
        // Retrieve the value associated with the key from preferences
        // If the key doesn't exist, return a default value (null in this case)
        return preferences.get(key, null);
        
    }
        
  
//register
     public static void insertUser(User user) {
    Connection connection = connect();
    if (connection != null) {
        try {
            // Check if the email already exists
            if (isEmailAlreadyUsed(user.getEmail())) {
                System.out.println("Email already used. Please choose a different email.");
                return; // Exit the method if email already exists
            }
            
            String sql = "INSERT INTO utilisateur (nom_utilisateur, adress_email, password) VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user.getFullName());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPassword());
            
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("User registered successfully!");
            } else {
                System.out.println("Failed to register user.");
            }

            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to register user!");
            e.printStackTrace();
        }
    }
}

public static boolean isEmailAlreadyUsed(String email) {
    Connection connection = connect();
    if (connection != null) {
        try {
            String sql = "SELECT COUNT(*) FROM utilisateur WHERE adress_email = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to check if email already exists!");
            e.printStackTrace();
        }
    }
    return false; // Return false by default or if an error occurs
}

//login****
    public static boolean login(String emaill,String passwordd){
        Connection connection = connect();
        if (connection != null) {
        try {
            String sql = "SELECT COUNT(*) FROM utilisateur WHERE adress_email = ? AND password = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, emaill);
            statement.setString(2, passwordd);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to check password");
            e.printStackTrace();
        }
    }
        return false;
    }
    public static boolean login_check_email(String emaill){
        Connection connection = connect();
        if (connection != null) {
        try {
            String sql = "SELECT COUNT(*) FROM utilisateur WHERE adress_email = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, emaill);
            //statement.setString(2, passwordd);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.err.println("Failed to check password");
            e.printStackTrace();
        }
    }
        return false;
    }

   public static void addtofavoris(String data, String title) {
    // Retrieve id_u based on email
    int id_u = Model.getIdUtilisateurByEmail(data);

    // Retrieve id_p based on title
    int id_p = Model.getIdPodcastByTitle(title);

    // Check if both id_u and id_p are valid
    if (id_u != -1 && id_p != -1) {
        // Insert into favoris table
        Model.insertIntoFavoris(id_u, id_p);
        System.out.println("Added podcast to favorites.");
    } else {
        System.out.println("User or podcast not found.");
    }
}

   
    public static void insertIntoFavoris(int id_utilisateur, int id_podcasts) {
        try {
            // Establish a database connection
            Connection conn = connect();

            // Prepare the SQL query with a prepared statement
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO favoris (date_add, id_utilisateur, id_podcasts) VALUES (CURRENT_DATE(), ?, ?)");

            // Set the id_utilisateur and id_podcasts parameters in the prepared statement
            pstmt.setInt(1, id_utilisateur);
            pstmt.setInt(2, id_podcasts);

            // Execute the update (insert) query
            pstmt.executeUpdate();

            // Close the prepared statement and connection
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            // Handle any SQL exceptions
            e.printStackTrace();
        }
    }
   
    // Assume that you have a method to establish a database connection in your Model class
// and execute SQL queries.

    public static int getIdUtilisateurByEmail(String email) {
        int id_utilisateur = -1; // Default value if not found
        
        try {
            // Establish a database connection
            Connection conn = connect();
            
            // Prepare the SQL query with a prepared statement
            String sql = "SELECT id_utilisateur FROM utilisateur WHERE adress_email = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            
            // Set the email parameter in the prepared statement
            pstmt.setString(1, email);
            
            // Execute the query
            ResultSet rs = pstmt.executeQuery();
            
            // Check if the result set has any rows
            if (rs.next()) {
                // Retrieve the id_utilisateur from the result set
                id_utilisateur = rs.getInt("id_utilisateur");
            }
            
            // Close the result set, prepared statement, and connection
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            // Handle any SQL exceptions
            e.printStackTrace();
        }
        
        return id_utilisateur;
    }


    
    
    public static int getIdPodcastByTitle(String title) {
        int id_podcast = -1; // Default value if not found

        try {
            // Establish a database connection
            Connection conn = connect();

            // Prepare the SQL query with a prepared statement
            PreparedStatement pstmt = conn.prepareStatement( "SELECT id_podcasts FROM podcasts WHERE title = ?");

            // Set the title parameter in the prepared statement
            pstmt.setString(1, title);

            // Execute the query
            ResultSet rs = pstmt.executeQuery();

            // Check if the result set has any rows
            if (rs.next()) {
                // Retrieve the id_podcast from the result set
                id_podcast = rs.getInt("id_podcasts");
            }

            // Close the result set, prepared statement, and connection
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            // Handle any SQL exceptions
            e.printStackTrace();
        }

        return id_podcast;
    }
 

public static List<Object[]> getDataFromJoinUserPodcastFromFavoris(String email) {
    List<Object[]> result = new ArrayList<>();

    try {
        // Establish a database connection
        Connection conn = connect();

        // Prepare the SQL query with a prepared statement
        String sql = "SELECT p.image, p.title, p.description " +
                     "FROM favoris f " +
                     "JOIN utilisateur u ON f.id_utilisateur = u.id_utilisateur " +
                     "JOIN podcasts p ON f.id_podcasts = p.id_podcasts " +
                     "WHERE u.adress_email = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        // Set the email parameter
        pstmt.setString(1, email);

        // Execute the query
        ResultSet rs = pstmt.executeQuery();

        // Iterate over the result set and retrieve data
        while (rs.next()) {
            String image = rs.getString("image");
            String title = rs.getString("title");
            String description = rs.getString("description");
            System.out.println("s");

            // Add the retrieved data to the result list
            result.add(new Object[]{image, title, description});
        }

        // Close the result set, prepared statement, and connection
        rs.close();
        pstmt.close();
        conn.close();
    } catch (SQLException e) {
        // Handle any SQL exceptions
        e.printStackTrace();
    }

    System.out.println("email:"+email);
    return result;
}
}
