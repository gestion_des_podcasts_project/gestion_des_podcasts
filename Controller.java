package Controler;

import gestion_podcasts_v2.Model.Model;
import gestion_podcasts_v2.Model.User;

public class Controller {
    public void registerUser(String fullName, String email, String password) {
        User user = new User(fullName, email, password);
        Model.insertUser(user);
    }
    public boolean isEmailAlreadyUsed(String email) {
        return Model.isEmailAlreadyUsed(email);
    }
    public static boolean login(String emaill,String passwordd){
        return Model.login(emaill, passwordd);
    }
    public static boolean login_check_email(String emaill){
        return Model.login_check_email(emaill);
    }
}
